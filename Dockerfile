FROM openjdk:11
ENV PROFILE dev
COPY target/k8s-demo-0.0.1-SNAPSHOT.jar /
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.profiles.active=${PROFILE}","-jar","k8s-demo-0.0.1-SNAPSHOT.jar"]
MAINTAINER LY
LABEL env=$PROFILE

# docker run --env PROFILE=lemon --rm -p 18080:8080 --name k8s-dem com.example/k8s-demo