package com.example.k8sdemo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@RestController
public class HelloController {
    private static Log logger= LogFactory.getLog(HelloController.class);


    @Value("Profile: ${spring.profiles.active}; Custom: ${k8s.hello}")
    private String hello;

    @RequestMapping(path = "/hello")
    public String hello(){
        logger.error("HelloController ERROR: "+hello);
        logger.info("HelloController INFO: "+hello);
        return hello;
    }

    @RequestMapping(path = "/bug")
    public void bug(){
        String str;
        int a=1/0;
    }
}
